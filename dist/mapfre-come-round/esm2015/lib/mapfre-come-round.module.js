/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MapfreComeRoundComponent } from './mapfre-come-round.component';
export class MapfreComeRoundModule {
}
MapfreComeRoundModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MapfreComeRoundComponent],
                imports: [],
                exports: [MapfreComeRoundComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwZnJlLWNvbWUtcm91bmQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbWFwZnJlLWNvbWUtcm91bmQvIiwic291cmNlcyI6WyJsaWIvbWFwZnJlLWNvbWUtcm91bmQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBUXpFLE1BQU0sT0FBTyxxQkFBcUI7OztZQU5qQyxRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsd0JBQXdCLENBQUM7Z0JBQ3hDLE9BQU8sRUFBRSxFQUNSO2dCQUNELE9BQU8sRUFBRSxDQUFDLHdCQUF3QixDQUFDO2FBQ3BDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hcGZyZUNvbWVSb3VuZENvbXBvbmVudCB9IGZyb20gJy4vbWFwZnJlLWNvbWUtcm91bmQuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbTWFwZnJlQ29tZVJvdW5kQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbTWFwZnJlQ29tZVJvdW5kQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBNYXBmcmVDb21lUm91bmRNb2R1bGUgeyB9XG4iXX0=