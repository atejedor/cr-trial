(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('mapfre-come-round', ['exports', '@angular/core'], factory) :
    (factory((global['mapfre-come-round'] = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MapfreComeRoundService = /** @class */ (function () {
        function MapfreComeRoundService() {
        }
        MapfreComeRoundService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        MapfreComeRoundService.ctorParameters = function () { return []; };
        /** @nocollapse */ MapfreComeRoundService.ngInjectableDef = i0.defineInjectable({ factory: function MapfreComeRoundService_Factory() { return new MapfreComeRoundService(); }, token: MapfreComeRoundService, providedIn: "root" });
        return MapfreComeRoundService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MapfreComeRoundComponent = /** @class */ (function () {
        function MapfreComeRoundComponent() {
        }
        /**
         * @return {?}
         */
        MapfreComeRoundComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        MapfreComeRoundComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'mcr-mapfre-come-round',
                        template: "<p>\r\n    hola mipulama\r\n</p>"
                    }] }
        ];
        /** @nocollapse */
        MapfreComeRoundComponent.ctorParameters = function () { return []; };
        return MapfreComeRoundComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MapfreComeRoundModule = /** @class */ (function () {
        function MapfreComeRoundModule() {
        }
        MapfreComeRoundModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [MapfreComeRoundComponent],
                        imports: [],
                        exports: [MapfreComeRoundComponent]
                    },] }
        ];
        return MapfreComeRoundModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.MapfreComeRoundService = MapfreComeRoundService;
    exports.MapfreComeRoundComponent = MapfreComeRoundComponent;
    exports.MapfreComeRoundModule = MapfreComeRoundModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=mapfre-come-round.umd.js.map