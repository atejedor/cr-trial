import { TestBed } from '@angular/core/testing';

import { MapfreComeRoundService } from './mapfre-come-round.service';

describe('MapfreComeRoundService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MapfreComeRoundService = TestBed.get(MapfreComeRoundService);
    expect(service).toBeTruthy();
  });
});
