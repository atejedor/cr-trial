/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of mapfre-come-round
 */
export { MapfreComeRoundService } from './lib/mapfre-come-round.service';
export { MapfreComeRoundComponent } from './lib/mapfre-come-round.component';
export { MapfreComeRoundModule } from './lib/mapfre-come-round.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21hcGZyZS1jb21lLXJvdW5kLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsdUNBQWMsaUNBQWlDLENBQUM7QUFDaEQseUNBQWMsbUNBQW1DLENBQUM7QUFDbEQsc0NBQWMsZ0NBQWdDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIG1hcGZyZS1jb21lLXJvdW5kXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvbWFwZnJlLWNvbWUtcm91bmQuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9tYXBmcmUtY29tZS1yb3VuZC5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbWFwZnJlLWNvbWUtcm91bmQubW9kdWxlJztcbiJdfQ==