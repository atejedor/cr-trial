import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MapfreComeRoundService {
    constructor() { }
}
MapfreComeRoundService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
MapfreComeRoundService.ctorParameters = () => [];
/** @nocollapse */ MapfreComeRoundService.ngInjectableDef = defineInjectable({ factory: function MapfreComeRoundService_Factory() { return new MapfreComeRoundService(); }, token: MapfreComeRoundService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MapfreComeRoundComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
MapfreComeRoundComponent.decorators = [
    { type: Component, args: [{
                selector: 'mcr-mapfre-come-round',
                template: "<p>\r\n    hola mipulama\r\n</p>"
            }] }
];
/** @nocollapse */
MapfreComeRoundComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MapfreComeRoundModule {
}
MapfreComeRoundModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MapfreComeRoundComponent],
                imports: [],
                exports: [MapfreComeRoundComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { MapfreComeRoundService, MapfreComeRoundComponent, MapfreComeRoundModule };

//# sourceMappingURL=mapfre-come-round.js.map