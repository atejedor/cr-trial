/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function MfcHeaderProperties() { }
if (false) {
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.name;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.logo;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.manageLinkLogo;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.logoUpperBar;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.title;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.hideTitle;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.titleStates;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.isFixed;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.subtitle;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.contactPhone;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.contactButtonLabel;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.contactButton;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.contactInfo;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.contactUs;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.contactLink;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.isLogoLink;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.iconLinks;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.logoPosition;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.upperBar;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.richedContent;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.classname;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.checkStyle;
    /** @type {?|undefined} */
    MfcHeaderProperties.prototype.logoImageAlt;
}
/**
 * @record
 */
export function IconLinksModel() { }
if (false) {
    /** @type {?|undefined} */
    IconLinksModel.prototype.icon;
    /** @type {?|undefined} */
    IconLinksModel.prototype.title;
    /** @type {?|undefined} */
    IconLinksModel.prototype.open;
    /** @type {?|undefined} */
    IconLinksModel.prototype.url;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmludGVyZmFjZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hlYWRlci8iLCJzb3VyY2VzIjpbImxpYi9oZWFkZXIuaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQSx5Q0FtQ0M7OztJQWxDRyxtQ0FBYzs7SUFDZCxtQ0FBYzs7SUFDZCw2Q0FJRTs7SUFDRiwyQ0FBc0I7O0lBQ3RCLG9DQUFlOztJQUNmLHdDQUFvQjs7SUFDcEIsMENBR0U7O0lBQ0Ysc0NBQWtCOztJQUNsQix1Q0FBa0I7O0lBQ2xCLDJDQUFzQjs7SUFDdEIsaURBQTRCOztJQUM1Qiw0Q0FBdUI7O0lBQ3ZCLDBDQUlFOztJQUNGLHdDQUFtQjs7SUFDbkIsMENBQXFCOztJQUNyQix5Q0FBb0I7O0lBQ3BCLHdDQUE2Qjs7SUFDN0IsMkNBQXNCOztJQUN0Qix1Q0FBbUI7O0lBQ25CLDRDQUFvQjs7SUFDcEIsd0NBQW1COztJQUNuQix5Q0FBaUI7O0lBQ2pCLDJDQUFtQjs7Ozs7QUFHdkIsb0NBS0M7OztJQUpHLDhCQUFjOztJQUNkLCtCQUFlOztJQUNmLDhCQUFjOztJQUNkLDZCQUFhIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBNZmNIZWFkZXJQcm9wZXJ0aWVzIHtcclxuICAgIG5hbWU/OiBzdHJpbmc7XHJcbiAgICBsb2dvPzogc3RyaW5nO1xyXG4gICAgbWFuYWdlTGlua0xvZ28/OiB7XHJcbiAgICAgICAgdGl0bGU/OiBzdHJpbmcsXHJcbiAgICAgICAgdHlwZT86IHN0cmluZyxcclxuICAgICAgICB0YXJnZXQ/OiBzdHJpbmdcclxuICAgIH07XHJcbiAgICBsb2dvVXBwZXJCYXI/OiBzdHJpbmc7XHJcbiAgICB0aXRsZT86IHN0cmluZztcclxuICAgIGhpZGVUaXRsZT86IGJvb2xlYW47XHJcbiAgICB0aXRsZVN0YXRlcz86IHtcclxuICAgICAgICBzdGF0ZXM/OiBzdHJpbmdbXSxcclxuICAgICAgICB0aXRsZXM/OiBzdHJpbmdbXVxyXG4gICAgfTtcclxuICAgIGlzRml4ZWQ/OiBib29sZWFuO1xyXG4gICAgc3VidGl0bGU/OiBzdHJpbmc7XHJcbiAgICBjb250YWN0UGhvbmU/OiBzdHJpbmc7XHJcbiAgICBjb250YWN0QnV0dG9uTGFiZWw/OiBzdHJpbmc7XHJcbiAgICBjb250YWN0QnV0dG9uPzogc3RyaW5nO1xyXG4gICAgY29udGFjdEluZm8/OiB7XHJcbiAgICAgICAgdGl0bGU/OiBzdHJpbmcsXHJcbiAgICAgICAgb3Blbj86IHN0cmluZyxcclxuICAgICAgICB1cmw/OiBzdHJpbmdcclxuICAgIH07XHJcbiAgICBjb250YWN0VXM/OiBzdHJpbmc7XHJcbiAgICBjb250YWN0TGluaz86IHN0cmluZztcclxuICAgIGlzTG9nb0xpbms/OiBzdHJpbmc7XHJcbiAgICBpY29uTGlua3M/OiBJY29uTGlua3NNb2RlbFtdO1xyXG4gICAgbG9nb1Bvc2l0aW9uPzogc3RyaW5nO1xyXG4gICAgdXBwZXJCYXI/OiBib29sZWFuO1xyXG4gICAgcmljaGVkQ29udGVudD86IGFueTtcclxuICAgIGNsYXNzbmFtZT86IHN0cmluZztcclxuICAgIGNoZWNrU3R5bGU/OiBhbnk7XHJcbiAgICBsb2dvSW1hZ2VBbHQ/OiBhbnk7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSWNvbkxpbmtzTW9kZWwge1xyXG4gICAgaWNvbj86IHN0cmluZztcclxuICAgIHRpdGxlPzogc3RyaW5nO1xyXG4gICAgb3Blbj86IHN0cmluZztcclxuICAgIHVybD86IHN0cmluZztcclxufVxyXG4iXX0=