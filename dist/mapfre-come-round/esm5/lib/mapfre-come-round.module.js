/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MapfreComeRoundComponent } from './mapfre-come-round.component';
var MapfreComeRoundModule = /** @class */ (function () {
    function MapfreComeRoundModule() {
    }
    MapfreComeRoundModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [MapfreComeRoundComponent],
                    imports: [],
                    exports: [MapfreComeRoundComponent]
                },] }
    ];
    return MapfreComeRoundModule;
}());
export { MapfreComeRoundModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwZnJlLWNvbWUtcm91bmQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbWFwZnJlLWNvbWUtcm91bmQvIiwic291cmNlcyI6WyJsaWIvbWFwZnJlLWNvbWUtcm91bmQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRXpFO0lBQUE7SUFNcUMsQ0FBQzs7Z0JBTnJDLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztvQkFDeEMsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsT0FBTyxFQUFFLENBQUMsd0JBQXdCLENBQUM7aUJBQ3BDOztJQUNvQyw0QkFBQztDQUFBLEFBTnRDLElBTXNDO1NBQXpCLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXBmcmVDb21lUm91bmRDb21wb25lbnQgfSBmcm9tICcuL21hcGZyZS1jb21lLXJvdW5kLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW01hcGZyZUNvbWVSb3VuZENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZXhwb3J0czogW01hcGZyZUNvbWVSb3VuZENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgTWFwZnJlQ29tZVJvdW5kTW9kdWxlIHsgfVxuIl19