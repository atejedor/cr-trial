import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var MapfreComeRoundService = /** @class */ (function () {
    function MapfreComeRoundService() {
    }
    MapfreComeRoundService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    MapfreComeRoundService.ctorParameters = function () { return []; };
    /** @nocollapse */ MapfreComeRoundService.ngInjectableDef = defineInjectable({ factory: function MapfreComeRoundService_Factory() { return new MapfreComeRoundService(); }, token: MapfreComeRoundService, providedIn: "root" });
    return MapfreComeRoundService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var MapfreComeRoundComponent = /** @class */ (function () {
    function MapfreComeRoundComponent() {
    }
    /**
     * @return {?}
     */
    MapfreComeRoundComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    MapfreComeRoundComponent.decorators = [
        { type: Component, args: [{
                    selector: 'mcr-mapfre-come-round',
                    template: "<p>\r\n    hola mipulama\r\n</p>"
                }] }
    ];
    /** @nocollapse */
    MapfreComeRoundComponent.ctorParameters = function () { return []; };
    return MapfreComeRoundComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var MapfreComeRoundModule = /** @class */ (function () {
    function MapfreComeRoundModule() {
    }
    MapfreComeRoundModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [MapfreComeRoundComponent],
                    imports: [],
                    exports: [MapfreComeRoundComponent]
                },] }
    ];
    return MapfreComeRoundModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { MapfreComeRoundService, MapfreComeRoundComponent, MapfreComeRoundModule };

//# sourceMappingURL=mapfre-come-round.js.map