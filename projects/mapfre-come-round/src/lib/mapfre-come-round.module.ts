import { NgModule } from '@angular/core';
import { MapfreComeRoundComponent } from './mapfre-come-round.component';

@NgModule({
  declarations: [MapfreComeRoundComponent],
  imports: [
  ],
  exports: [MapfreComeRoundComponent]
})
export class MapfreComeRoundModule { }
