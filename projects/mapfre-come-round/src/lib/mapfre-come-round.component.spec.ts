import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapfreComeRoundComponent } from './mapfre-come-round.component';

describe('MapfreComeRoundComponent', () => {
  let component: MapfreComeRoundComponent;
  let fixture: ComponentFixture<MapfreComeRoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapfreComeRoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapfreComeRoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
