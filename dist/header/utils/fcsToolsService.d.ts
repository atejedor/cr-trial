export declare class FcsTools {
    static findObjectInObject(object: any, name: any, deepLevel: any, ignoreLevels: any, getParent?: any): any;
    static isEmpty(param: any): boolean;
}
