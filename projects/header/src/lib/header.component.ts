import { FcsTools } from './../utils/fcsToolsService';
import { Component, OnInit, ElementRef, ViewChild, Renderer2, Input } from '@angular/core';
import { isString, isUndefined } from 'util';
import { MfcHeaderProperties } from './header.interface';

@Component({
  selector: 'mfc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() mfcProperties: MfcHeaderProperties;

  private title: string;
  private isLogoLink;

  // temp vars

  public logoUpperBarImageAlt: any;

  @ViewChild('contactList') contactList: ElementRef;

  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.isLogoLink = this.mfcProperties.manageLinkLogo
      && this.mfcProperties.manageLinkLogo.type
      && this.mfcProperties.manageLinkLogo.target;

    if (!isString(this.title)) {
      this.title = this.mfcProperties.title;
    }
    if (this.mfcProperties.richedContent) {
      this.mfcProperties.richedContent = { content: this.mfcProperties.richedContent, visibleOnMobile: true };
    }

    if (this.mfcProperties.isFixed) {
      // _resize();
      // ng.element($window).bind('resize', _resize);
    }

    // scope.popup = popup;
    this.mfcProperties.checkStyle = this.checkStyle;
    // scope.openIconLinks = openIconLinks;
    // scope.manageLink = manageLink;

    // scope.$watch(checkState, updateTitle);
    // scope.$watch(watchRnr, updateContactPhone);

  }

  _resize() {
    // var headerHeight = el.find('.mfc-header').height();
    // ng.element('.mfc-layout__main-container').css('marginTop', headerHeight);
  }

  // checkState() {
  //   return $state.current.name;
  // }

  updateTitle(newValue) {
    let indexTitle = -1;
    if (!isUndefined(this.mfcProperties.titleStates)
      && (this.mfcProperties.titleStates.states.length
        === this.mfcProperties.titleStates.titles.length)) {
      indexTitle = this.mfcProperties.titleStates.states.indexOf(newValue);
    }

    if (indexTitle > -1) {
      this.mfcProperties.title = this.mfcProperties.titleStates.titles[indexTitle];
    } else {
      this.mfcProperties.title = this.title;
    }
  }

  watchRnr() {
    // return mfcGlobalData.server.rnr;
  }

  updateContactPhone(rnr) {
    if (!FcsTools.isEmpty(rnr)
      && !FcsTools.isEmpty(rnr.office)
      && isString(this.mfcProperties.contactPhone)
      && isString(rnr.office.phone)) {
      this.mfcProperties.contactPhone = rnr.office.phone;
    }
  }

  popup(url) {
    const w = 700;
    const h = 300;
    const LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
    const TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
    const settings = 'height=' + h + ',width=' + w + ',top=' +
      TopPosition + ',left=' + LeftPosition + ',scrollbars=' + 'yes' +
      ',resizable';

    window.open(url, 'popUpWindow', settings);
  }

  checkStyle(title) {
    if (title && this.contactList) {
      this.renderer.addClass(this.contactList.nativeElement, 'mfc-header__content__contact__list--up');
    }
  }

  openIconLinks(type, link) {
    let busEvent = {};

    if (type === 'shadow-box') {
      busEvent = {
        from: this.mfcProperties.name,
        to: link,
        action: 'open'
      };

      // scope.$emit("bus:shadow-box:up", busEvent);
    } else {
      window.open(link, type);
    }
  }

  manageLink(type, target) {
    // const value;
    // const focus;
    // let options;

    // options = {
    //   scope: scope,
    //   form: form,
    //   name: scope.name,
    //   value: value,
    //   focus: focus
    // };
    // mfcTools.manageLinks(type, target, options);
  }

}
